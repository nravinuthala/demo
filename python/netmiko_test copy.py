from netmiko import ConnectHandler
switches = ["192.168.255.153", "192.168.255.154"]
routers = ["192.168.255.150", "192.168.255.151"]
devices = [switches, routers]
csr_1000 = "192.168.255.151"

print(devices)

def find_prompt():

    for device_ip in devices:
        device = ConnectHandler(
            device_type = "cisco_ios",
            host = device_ip,
            username = "admin",
            password = "cisco"
        )

        print(device.find_prompt())

def show_intfc_details():
    for device_ip in devices:
        device = ConnectHandler(
            device_type = "cisco_ios",
            host = device_ip,
            username = "admin",
            password = "cisco"
        )
        print(device.send_command("sh ip int br"))

def config_rest_api(router_ip, enable):
    router = ConnectHandler(
        device_type = "cisco_xe",
        host = router_ip,
        username = "admin",
        password = "cisco"
    )

    config_commands = [ 'remote-management',
                    'restful-api',
                    'end' ]
    if not enable :
        config_commands[1] = 'no ' + config_commands[1]
    output = router.send_config_set(config_commands)
    print(output)

def create_loopback(router_ip, enable):
    router = ConnectHandler(
        device_type = "cisco_xe",
        host = router_ip,
        username = "admin",
        password = "cisco"
    )
    config_commands_enable = [ 'interface Loopback 0',
                    'ip address 192.168.0.170 255.255.255.0',
                    'do sh ip int br',
                    'end',
                    'wr' ]

    config_commands_disable = [ 'no interface Loopback 0',
                    'do sh ip int br',
                    'end',
                    'wr' ]
    config_commands = config_commands_enable
    if not enable :
        config_commands = config_commands_disable
    output = router.send_config_set(config_commands)
    print(output)
    
# config_rest_api(csr_1000, True)
# create_loopback(csr_1000, False)
show_intfc_details()