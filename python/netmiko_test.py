from netmiko import ConnectHandler
switches = ["192.168.255.153", "192.168.255.154"]
routers = ["192.168.255.150", "192.168.255.151"]
devices = [*switches, *routers]
csr_1000 = "192.168.255.151"

def show_intfc_details():
    for device_ip in devices:
        device = ConnectHandler(
            device_type = "cisco_ios",
            host = device_ip,
            username = "admin",
            password = "cisco"
        )
        print(device.send_command("sh ip int br"))

def create_loopback(router_ip, enable):
    router = ConnectHandler(
        device_type = "cisco_xe",
        host = router_ip,
        username = "admin",
        password = "cisco"
    )
    config_commands_enable = [ 'interface Loopback 0',
                    'ip address 192.168.0.170 255.255.255.0',
                    'do sh ip int br',
                    'end',
                    'wr' ]

    config_commands_disable = [ 'no interface Loopback 0',
                    'do sh ip int br',
                    'end',
                    'wr' ]
    config_commands = config_commands_enable
    if not enable :
        config_commands = config_commands_disable
    output = router.send_config_set(config_commands)
    print(output)

# show_intfc_details()    
create_loopback(csr_1000, True)
